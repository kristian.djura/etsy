package automation.drivers.strategies;

import automation.utils.Constants;

public class DriverStrategyImplementor {

    public static DriverStrategy choseStrategy(String strategy) {

        switch (strategy) {
            case Constants.CHROME:
                return new Chrome();

            case Constants.FIREFOX:
                return new Firefox();

            default:
                return null;
        }
    }
}
