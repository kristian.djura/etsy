package automation.utils;

public class Constants {

    public static final String PROP_FILE_NAME = "framework.properties";
    public static final String FILE_NOT_FOUND = "The property file has not been found";
    public static final String CHROME = "Chrome";
    public static final String FIREFOX = "Firefox";
    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";
    public static final String URL = "https://www.etsy.com/";
    public static final int TIMEOUT = 5;
    public static final String PETBEDDINGLINK = "c/pet-supplies/pet-bedding?ref=catnav-891";
    public static final String CLTHANDSHSLINK = "c/clothing-and-shoes?ref=catnav-10923";
    public static final String ITEM1 = "Cat Suckling Pillow Cover- Black's - Cat Pacifier- Catsifier";
    public static final String PATTERN = "Owl w/ Tan";
    public static final String INPUTPRICEMIN = "90";
    public static final String INPUTPRICEMAX = "100";
    public static final String LOCATION = "Belgium";
    public static final String ITEM2 = "High sneakers for men with military camouflage print. Khaki color. Sneakers with laces. Comfortable shoes of high quality";
    public static final String SHOESSIZE = "US 10";
    public static final String NUMBEROFITEMS2 = "2 items in your cart";
    public static final String NUMBEROFITEMS1 = "1 item in your cart";
    public static final String NAME = "name";
}
