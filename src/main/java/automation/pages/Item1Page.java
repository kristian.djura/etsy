package automation.pages;

import automation.drivers.DriverSingleton;
import automation.utils.Constants;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;

public class Item1Page {

    WebDriver driver;
    WebDriverWait wait;

    public Item1Page(){

        driver = DriverSingleton.getDriver();
        PageFactory.initElements(driver, this);
        wait = new WebDriverWait(driver, Constants.TIMEOUT);
    }

    @FindBy(id = "inventory-variation-select-0")
    private WebElement dropDown;


    @FindBy(css = "#listing-page-cart > div.wt-mb-lg-0.wt-mb-xs-6 > div.wt-display-flex-xs.wt-flex-direction-column-xs.wt-flex-direction-row-md.wt-flex-wrap.wt-flex-direction-column-lg > div > form > button")
    private WebElement addToCartButton;



    public void addingItemToCart() throws InterruptedException {

        Select dropDownMenu1 = new Select(dropDown);
        dropDownMenu1.selectByVisibleText(Constants.PATTERN);

        Thread.sleep(2000);

        addToCartButton.click();

        driver.close();

        ArrayList<String> newTab = new ArrayList<>(driver.getWindowHandles());
        driver.switchTo().window(newTab.get(0));

        driver.get(Constants.URL);
    }
}
