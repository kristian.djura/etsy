package automation.pages;

import automation.drivers.DriverSingleton;
import automation.utils.Constants;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


import java.util.Random;



public class HomePage {

    private WebDriver driver;
    private WebDriverWait wait;

    public HomePage(){

        this.driver = DriverSingleton.getDriver();
        PageFactory.initElements(driver, this);
        wait = new WebDriverWait(driver, Constants.TIMEOUT);
    }

    @FindBy(css = "#gnav-header-inner > div.wt-flex-shrink-xs-0 > nav > ul > li:nth-child(1) > button")
    private WebElement signInButtonOnWebpage;

    @FindBy(id = "join_neu_email_field")
    private WebElement emailField;

    @FindBy(css = "#join-neu-form > div.wt-grid.wt-grid--block > div > div:nth-child(8) > div > button")
    private WebElement continueButton;

    @FindBy(id = "join_neu_first_name_field")
    private WebElement firstNameField;


    @FindBy(id = "join_neu_password_field")
    private WebElement passwordField;

    @FindBy(css = "#join-neu-form > div.wt-grid.wt-grid--block > div > div:nth-child(9) > div > button")
    private WebElement signInButtonInWindow;

    @FindBy(css = "#join-neu-form > div.wt-grid.wt-grid--block > div > div:nth-child(8) > div > button")
    private WebElement registerButton;

    @FindBy(css = "#join-neu-form > div.wt-grid.wt-grid--block > div > div:nth-child(1) > div > button")
    private WebElement registerButtonInWindow;

    @FindBy(css = "#desktop-category-nav > div.wt-bg-white.wt-hide-xs.wt-show-lg.wt-text-caption.wt-position-relative.wt-z-index-4.v2-toolkit-cat-nav-tab-bar > div > ul > li:nth-child(3) > a")
    private WebElement homeAndLivingSection;

    @FindBy(css = "#desktop-category-nav > div.wt-position-relative.wt-body-max-width > div > div:nth-child(3) > div > div > aside > ul > li.side-nav-item.wt-p-xs-0.wt-display-block.active")
    private WebElement petSupliesSection;

    @FindBy(css = "#gnav-header-inner > div.wt-flex-shrink-xs-0 > nav > ul > li:nth-child(2) > span > a")
    private WebElement cart;

//    @FindBy(id = "catnav-l3-4442")
//    private WebElement petBeddingSection;

    @FindBy(css = "#join-neu-overlay-title")
    private WebElement titleText;


    public void registerUser(String name, String password) throws InterruptedException {

        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 7) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();

        String email = saltStr+"@"+"mail"+".com";

        signInButtonOnWebpage.click();
        emailField.sendKeys(email);

        try{
            continueButton.click();
            Thread.sleep(2000);
            firstNameField.sendKeys(name);
            passwordField.sendKeys(password);
            registerButton.click();
        }catch (Exception e){

            registerButtonInWindow.click();
            Thread.sleep(2000);
            emailField.sendKeys(email);
            firstNameField.sendKeys(name);
            passwordField.sendKeys(password);
            registerButton.click();

        }
    }



    public void signInUser(String email, String password){


        signInButtonOnWebpage.click();
        emailField.sendKeys(email);
        try{
            continueButton.click();
            passwordField.sendKeys(password);
            signInButtonInWindow.click();
        }catch (Exception e){

            passwordField.sendKeys(password);
            signInButtonInWindow.click();
        }


        }

    public PetBeddingPage goToPetBeddingPage(){

        driver.get(Constants.URL+Constants.PETBEDDINGLINK);

        WebElement onSaleCheck;
        onSaleCheck = driver.findElement(By.cssSelector("#search-filter-reset-form > div:nth-child(2) > fieldset > div > div > div:nth-child(2)"));

        wait.until(ExpectedConditions.elementToBeClickable(onSaleCheck));

        return new PetBeddingPage();
    }

    public ClothingAndShoesPage goToClothingAndShoesPage(){

        driver.get(Constants.URL+Constants.CLTHANDSHSLINK);

        WebElement onSaleCheck;
        onSaleCheck = driver.findElement(By.cssSelector("#search-filter-reset-form > div:nth-child(2) > fieldset > div > div > div:nth-child(2)"));

        wait.until(ExpectedConditions.elementToBeClickable(onSaleCheck));

        return new ClothingAndShoesPage();
    }

    public void goToCart(){

        cart.click();
    }

}
