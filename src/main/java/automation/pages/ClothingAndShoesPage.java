package automation.pages;

import automation.drivers.DriverSingleton;
import automation.utils.Constants;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.lang.constant.Constable;
import java.util.ArrayList;

public class ClothingAndShoesPage {

    WebDriver driver;
    WebDriverWait wait;

    public ClothingAndShoesPage(){

        driver = DriverSingleton.getDriver();
        PageFactory.initElements(driver, this);
        wait = new WebDriverWait(driver, Constants.TIMEOUT);
    }

    @FindBy(id = "search-filter-min-price-input")
    private WebElement inputPriceMin;

    @FindBy(id = "search-filter-max-price-input")
    private WebElement inputPriceMax;

    @FindBy(css = "#search-filter-reset-form > div:nth-child(4) > fieldset > div > div > div:nth-child(6) > div.wt-grid.wt-ml-xs-4.wt-nudge-b-4 > div.wt-grid__item-xs-2.wt-ml-xs-1 > button")
    private WebElement confirmArrowPrice;

    @FindBy(xpath = "/html/body/div[5]/div/div[1]/div/div[4]/div[1]/div/div/form/div[5]/fieldset/div/div/div[4]/div[2]/input")
    private WebElement enterLocation;

    @FindBy(css = "#search-filter-reset-form > div:nth-child(6) > fieldset > div > div > div:nth-child(4) > div.wt-display-flex-md.wt-grid.wt-mt-md-1.wt-ml-xs-4 > div > button")
    private WebElement confirmArrowLocation;

    @FindBy(css = "#search-filter-reset-form > div:nth-child(7) > fieldset > div > div > div:nth-child(2) > a")
    private WebElement handmadeRadioButton;


    public void filteringSecondItem(String inputMin, String inputMax, String location){

        inputPriceMin.sendKeys(inputMin);
        inputPriceMax.sendKeys(inputMax);
        confirmArrowPrice.click();

        wait.until(ExpectedConditions.elementToBeClickable(enterLocation));
        enterLocation.sendKeys(location);
        confirmArrowLocation.click();

        wait.until(ExpectedConditions.elementToBeClickable(handmadeRadioButton));
        handmadeRadioButton.click();

        ArrayList<WebElement> listOfItems;
        listOfItems = (ArrayList<WebElement>) driver.findElements(By.xpath("//*[@id=\"content\"]/div/div[1]/div/div[4]/div[2]/div[2]/div[3]/div/div/ul/li"));


        for (int i=0; i<listOfItems.size()+1; i++){

            if (listOfItems.get(i).getText().contains(Constants.ITEM2)){

                listOfItems.get(i).click();

                break;
            }
        }

        ArrayList<String> newTab = new ArrayList<>(driver.getWindowHandles());
        driver.switchTo().window(newTab.get(1));
    }
}
