package automation.pages;

import automation.drivers.DriverSingleton;
import automation.utils.Constants;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import javax.swing.*;
import java.util.ArrayList;

public class PetBeddingPage {

    WebDriver driver;
    WebDriverWait wait;

    public PetBeddingPage(){

        driver = DriverSingleton.getDriver();
        PageFactory.initElements(driver, this);
        wait = new WebDriverWait(driver, Constants.TIMEOUT);
    }

    @FindBy(css = "#search-filter-reset-form > div:nth-child(2) > fieldset > div > div > div:nth-child(2) > div > a")
    private WebElement filterOnSaleCheckButton;

    @FindBy(css = "#search-filter-reset-form > div:nth-child(4) > fieldset > div > div > div:nth-child(3) > a")
    private WebElement filterPriceRadioButton;

    @FindBy(css = "#search-filter-reset-form > fieldset:nth-child(5) > div > div:nth-child(1) > div:nth-child(2) > div > a")
    private WebElement filterColorCheckButtonBlack;

    @FindBy(css = "#search-filter-reset-form > fieldset:nth-child(5) > div > div:nth-child(1) > div:nth-child(3) > div > a")
    private WebElement filterColorCheckButtonBlue;

    @FindBy(css = "#content > div > div.wt-bg-white.wt-grid__item-md-12.wt-pl-xs-1.wt-pr-xs-0.wt-pr-md-1.wt-pl-lg-0.wt-pr-lg-0.wt-bb-xs-1 > div > div.wt-mt-xs-2.wt-text-black > div.wt-grid.wt-pl-xs-0.wt-pr-xs-1.search-listings-group > div:nth-child(2) > div.wt-bg-white.wt-display-block.wt-pb-xs-2.wt-mt-xs-0 > div > div > ul > li:nth-child(1)")
    private WebElement firstElementFromTheList;

    @FindBy(css = "#content > div > div.wt-bg-white.wt-grid__item-md-12.wt-pl-xs-1.wt-pr-xs-0.wt-pr-md-1.wt-pl-lg-0.wt-pr-lg-0.wt-bb-xs-1 > div > div.wt-mt-xs-2.wt-text-black > div.wt-grid.wt-pl-xs-0.wt-pr-xs-1.search-listings-group > div:nth-child(2) > div.wt-bg-white.wt-display-block.wt-pb-xs-2.wt-mt-xs-0 > div > div > ul > li:nth-child(1) > div > a")
    private WebElement firstElementFav;

    @FindBy(css = "#content > div > div.wt-bg-white.wt-grid__item-md-12.wt-pl-xs-1.wt-pr-xs-0.wt-pr-md-1.wt-pl-lg-0.wt-pr-lg-0.wt-bb-xs-1 > div > div.wt-mt-xs-2.wt-text-black > div.wt-grid.wt-pl-xs-0.wt-pr-xs-1.search-listings-group > div:nth-child(2) > div.wt-bg-white.wt-display-block.wt-pb-xs-2.wt-mt-xs-0 > div > div > ul > li:nth-child(2) > div > a")
    private WebElement secondElementFav;

    @FindBy(css = "#content > div > div.wt-bg-white.wt-grid__item-md-12.wt-pl-xs-1.wt-pr-xs-0.wt-pr-md-1.wt-pl-lg-0.wt-pr-lg-0.wt-bb-xs-1 > div > div.wt-mt-xs-2.wt-text-black > div.wt-grid.wt-pl-xs-0.wt-pr-xs-1.search-listings-group > div:nth-child(2) > div.wt-bg-white.wt-display-block.wt-pb-xs-2.wt-mt-xs-0 > div > div > ul > li:nth-child(3) > div > a")
    private WebElement thirdElementFav;

    @FindBy(css = "#content > div > div.wt-bg-white.wt-grid__item-md-12.wt-pl-xs-1.wt-pr-xs-0.wt-pr-md-1.wt-pl-lg-0.wt-pr-lg-0.wt-bb-xs-1 > div > div.wt-mt-xs-2.wt-text-black > div.wt-grid.wt-pl-xs-0.wt-pr-xs-1.search-listings-group > div:nth-child(2) > div.wt-bg-white.wt-display-block.wt-pb-xs-2.wt-mt-xs-0 > div > div > ul > li:nth-child(1) > div > div > button")
    private WebElement firstElementFavButton;

    @FindBy(css = "#content > div > div.wt-bg-white.wt-grid__item-md-12.wt-pl-xs-1.wt-pr-xs-0.wt-pr-md-1.wt-pl-lg-0.wt-pr-lg-0.wt-bb-xs-1 > div > div.wt-mt-xs-2.wt-text-black > div.wt-grid.wt-pl-xs-0.wt-pr-xs-1.search-listings-group > div:nth-child(2) > div.wt-bg-white.wt-display-block.wt-pb-xs-2.wt-mt-xs-0 > div > div > ul > li:nth-child(2) > div > div > button")
    private WebElement secondElementFavButton;

    @FindBy(css = "#content > div > div.wt-bg-white.wt-grid__item-md-12.wt-pl-xs-1.wt-pr-xs-0.wt-pr-md-1.wt-pl-lg-0.wt-pr-lg-0.wt-bb-xs-1 > div > div.wt-mt-xs-2.wt-text-black > div.wt-grid.wt-pl-xs-0.wt-pr-xs-1.search-listings-group > div:nth-child(2) > div.wt-bg-white.wt-display-block.wt-pb-xs-2.wt-mt-xs-0 > div > div > ul > li:nth-child(3) > div > div > button")
    private WebElement thirdElementFavButton;


    public void filteringAndAddingFirstItemToCart(){

        filterOnSaleCheckButton.click();
        wait.until(ExpectedConditions.elementToBeClickable(filterPriceRadioButton));
        filterPriceRadioButton.click();

        wait.until(ExpectedConditions.elementToBeClickable(filterColorCheckButtonBlack));
        filterColorCheckButtonBlack.click();

        wait.until(ExpectedConditions.elementToBeClickable(filterColorCheckButtonBlue));
        filterColorCheckButtonBlue.click();

//        wait.until(ExpectedConditions.elementToBeClickable(firstElementFromTheList));
//        firstElementFromTheList.click();

        ArrayList<WebElement> listOfItems;
        listOfItems = (ArrayList<WebElement>) driver.findElements(By.xpath("//*[@id=\"content\"]/div/div[1]/div/div[4]/div[2]/div[2]/div[3]/div/div/ul/li"));

        //*[@id="content"]/div/div[1]/div/div[4]/div[2]/div[2]/div[3]/div/div/ul/li

        for (int i=0; i<listOfItems.size()+1; i++){

            if (listOfItems.get(i).getText().contains(Constants.ITEM1)){

                listOfItems.get(i).click();

                break;
            }
        }

        ArrayList<String> newTab = new ArrayList<>(driver.getWindowHandles());
        driver.switchTo().window(newTab.get(1));
    }

    public void addingItemsToFavorites(){

        Actions hover = new Actions(driver);
        hover.moveToElement(firstElementFav);

        firstElementFavButton.click();
        hover.moveToElement(secondElementFav);

        secondElementFav.click();
        hover.moveToElement(thirdElementFav);

        thirdElementFav.click();

    }
}
