package automation.pages;

import automation.drivers.DriverSingleton;
import automation.utils.Constants;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;

public class Cart {

    WebDriver driver;
    WebDriverWait wait;

    public Cart() {

        driver = DriverSingleton.getDriver();
        PageFactory.initElements(driver, this);
        wait = new WebDriverWait(driver, Constants.TIMEOUT);
    }

    @FindBy(css = "#multi-shop-cart-list > div > div > div.wt-grid.wt-position-relative.wt-pl-xs-0.wt-pr-xs-0 > ul > li:nth-child(2) > div.wt-grid.wt-mb-xs-1.wt-mb-lg-2 > div:nth-child(1) > fieldset > div.wt-checkbox.wt-checkbox--small")
    private WebElement thisIsAGiftButton;

    @FindBy(css = "#multi-shop-cart-list > div > div > div.wt-grid.wt-position-relative.wt-pl-xs-0.wt-pr-xs-0 > ul > li:nth-child(2) > div.wt-grid.wt-mb-xs-1.wt-mb-lg-2 > div:nth-child(1) > fieldset > div:nth-child(3) > div.wt-checkbox.wt-checkbox--small.wt-mb-xs-2")
    private WebElement addGiftMsgButton;

    @FindBy(css = "#multi-shop-cart-list > div > div > div.wt-grid.wt-position-relative.wt-pl-xs-0.wt-pr-xs-0 > ul > li:nth-child(2) > div.wt-grid.wt-mb-xs-1.wt-mb-lg-2 > div:nth-child(1) > fieldset > div:nth-child(3) > div:nth-child(2)")
    private WebElement enterMsg;

    @FindBy(css = "#multi-shop-cart-list > div > div > div.wt-grid.wt-position-relative.wt-pl-xs-0.wt-pr-xs-0 > ul > li:nth-child(1) > ul > li > div > div.wt-flex-xs-3.wt-pl-xs-2.wt-pl-lg-3 > div > div.wt-grid__item-xs-12.wt-grid__item-lg-7 > div:nth-child(5) > ul > li.wt-list-inline__item.wt-pb-xs-2.wt-pr-xs-1")
    private WebElement removeButton;

    @FindBy(className = "wt-text-heading-01")
    private WebElement numberOfItemsInCart;

    public void checkingForTextBox() {

        thisIsAGiftButton.click();
        wait.until(ExpectedConditions.elementToBeClickable(addGiftMsgButton));

        addGiftMsgButton.click();

        wait.until(ExpectedConditions.elementToBeClickable(enterMsg));

    }

    public boolean checkIfTextBoxIsDisplayed() {

        return enterMsg.isDisplayed();
    }


    public void unchekigTheTextBox() {

        addGiftMsgButton.click();

        thisIsAGiftButton.click();
    }

    public void removingItem() throws InterruptedException {

        removeButton.click();

        Thread.sleep(2000);
    }

    public boolean validatingNumberOfItemsInCart(){

        System.out.println(numberOfItemsInCart.getText());

        return numberOfItemsInCart.getText().contains(Constants.NUMBEROFITEMS1);
    }
}





