import automation.drivers.DriverSingleton;
import automation.pages.*;
import automation.utils.Constants;
import automation.utils.FrameworkProperties;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

public class Tests {

    static HomePage homePage;
    static WebDriver driver;
    static PetBeddingPage petBeddingPage;
    static FrameworkProperties frameworkProperties;
    static Item1Page item1Page;
    static ClothingAndShoesPage clothingAndShoesPage;
    static Item2Page item2Page;
    static Cart cart;

    @Before
    public void initObjects(){

        DriverSingleton.getInstance(Constants.CHROME);
        driver = DriverSingleton.getDriver();
        homePage = new HomePage();
        petBeddingPage = new PetBeddingPage();
        frameworkProperties = new FrameworkProperties();
        item1Page = new Item1Page();
        clothingAndShoesPage = new ClothingAndShoesPage();
        item2Page = new Item2Page();
        cart = new Cart();
    }

    //Test Case 1, Registering on Etsy website with e-mail
    @Test
    public void test01RegisteringUser() throws InterruptedException {

        driver.get(Constants.URL);
        driver.navigate().refresh();
        homePage.registerUser(frameworkProperties.getProperty(Constants.NAME), frameworkProperties.getProperty(Constants.PASSWORD));
    }

    //Test Case 3, Log in to Etsy website
    @Test
    public void test02LoggingInUser(){
        driver.get(Constants.URL);
        homePage.signInUser(frameworkProperties.getProperty(Constants.EMAIL), frameworkProperties.getProperty(Constants.PASSWORD));
    }

    //Test Case 6, Filtering and adding items to cart
    @Test
    public void test03FilterAndAddItemsToCart() throws InterruptedException {

        driver.get(Constants.URL);
//        homePage.registerUser(frameworkProperties.getProperty(Constants.NAME), frameworkProperties.getProperty(Constants.PASSWORD));
        homePage.goToPetBeddingPage();
        petBeddingPage.filteringAndAddingFirstItemToCart();
        item1Page.addingItemToCart();
        homePage.goToClothingAndShoesPage();
        clothingAndShoesPage.filteringSecondItem(Constants.INPUTPRICEMIN, Constants.INPUTPRICEMAX, Constants.LOCATION);
        item2Page.addingSecondItemToCart();

        Assert.assertEquals(item2Page.checkNumberOfItems(), true);
    }

    //Test Case 7, Managing cart, setting item as a gift and removing item
    @Test
    public void test04ManagingCart() throws InterruptedException {

        driver.get(Constants.URL);
        homePage.goToPetBeddingPage();
        petBeddingPage.filteringAndAddingFirstItemToCart();
        item1Page.addingItemToCart();
        homePage.goToClothingAndShoesPage();
        clothingAndShoesPage.filteringSecondItem(Constants.INPUTPRICEMIN, Constants.INPUTPRICEMAX, Constants.LOCATION);
        item2Page.addingSecondItemToCart();
        item2Page.closingTab();
        homePage.goToCart();
        cart.checkingForTextBox();

        Assert.assertEquals(cart.checkIfTextBoxIsDisplayed(), true);

        cart.unchekigTheTextBox();

        Assert.assertEquals(cart.checkIfTextBoxIsDisplayed(), false);

        cart.removingItem();

        Assert.assertEquals(cart.validatingNumberOfItemsInCart(), true);
    }

    //Test Case 9, Adding items to favorites
    @Test
    public void test05AddingItemsToFavorires() throws InterruptedException {

        driver.get(Constants.URL);
        homePage.registerUser(frameworkProperties.getProperty(Constants.NAME), frameworkProperties.getProperty(Constants.PASSWORD));
        driver.get(Constants.URL);
        homePage.goToPetBeddingPage();
        petBeddingPage.addingItemsToFavorites();

    }

}
